module main_data
implicit none
    real(8),save::h=1d0,omega=5d0
    
    real(8),save::C(6,6),l(3,6),epsilon(3,3),rho=4.63d0
    
    real(8),save::epsilon0=8.85418781762039d-3
    
    public::create_data,C,l,epsilon,rho,h,omega,epsilon0
    contains
    
    subroutine create_data
    implicit none
        integer(4) i,j
        
        real(8) v
        
        do i=1,6
            do j=1,6
                C(i,j)=0d0
            enddo
        enddo
        do i=1,3
            do j=1,6
                l(i,j)=0d0
            enddo
        enddo
        do i=1,3
            do j=1,3
                epsilon(i,j)=0d0
            enddo
        enddo
        
        !is Y side
        C(1,1)=270d0
        C(1,3)=101d0
        C(2,1)=96d0
        C(2,2)=226d0
        C(2,3)=68d0
        C(3,3)=186d0
        C(4,4)=25d0
        C(5,5)=74.3d0
        C(6,6)=95.5d0
        
        l(1,5)=11.7d0
        l(2,4)=5.16d0
        l(3,1)=-1.1d0
        l(3,2)=2.46d0
        l(3,3)=4.4d0
        
        epsilon(1,1)=780d0
        epsilon(2,2)=34d0
        epsilon(3,3)=24d0
        
        !is X side
        if(.true.) then
            v=C(4,4)
            C(4,4)=C(5,5)
            C(5,5)=v
            
            v=l(1,5)
            l(1,5)=l(2,4)
            l(2,4)=v
            
            v=epsilon(1,1)
            epsilon(1,1)=epsilon(2,2)
            epsilon(2,2)=v
        endif
        
        do i=1,6
            do j=1,6
                if(abs(C(i,j))<1e-7) C(i,j)=C(j,i)
                if(abs(C(j,i))<1e-7) C(j,i)=C(i,j)
            enddo
        enddo
    endsubroutine create_data
endmodule main_data