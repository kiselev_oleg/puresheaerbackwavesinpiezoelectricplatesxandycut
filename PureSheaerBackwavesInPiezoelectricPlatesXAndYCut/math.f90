module math
implicit none
    
    public::ch,sh,th
    contains
    complex(8) function ch(x) result(f)
    implicit none
        complex(8),intent(in)::x
        
        f=(exp(x)+exp(-x))*0.5d0
    endfunction ch
    complex(8) function sh(x) result(f)
    implicit none
        complex(8),intent(in)::x
        
        f=(exp(x)-exp(-x))*0.5d0
    endfunction sh
    complex(8) function th(x) result(f)
    implicit none
        complex(8),intent(in)::x
        
        f=sh(x)/ch(x)
    endfunction th
endmodule math