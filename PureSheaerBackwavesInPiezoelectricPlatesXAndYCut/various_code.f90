module various_code
implicit none

    public::create_graphics_of_phase_speed
    contains
    subroutine create_graphics_of_phase_speed
    use group_and_phase_speed,only:get_phase_speed
    use main_data,only:omega
    implicit none
        real(8),allocatable::sp(:)
        
        integer(4) i
        
        open(1,file="graphics/phase_speeds.txt")
        omega=1d-7
        do while(omega<70d0)
            call get_phase_speed(sp)
            
            do i=1,size(sp)
                write(1,*),omega,sp(i)
            enddo
            
            deallocate(sp)
            
            omega=omega+1d-3
        enddo
        close(1)
    endsubroutine create_graphics_of_phase_speed
endmodule various_code