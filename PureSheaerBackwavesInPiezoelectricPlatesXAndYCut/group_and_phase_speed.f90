module group_and_phase_speed
implicit none
    complex(8),save::psi(4),G(4)
    real(8),save::k_sim(25),k_asim(25)
    integer(4),save::size_k_sim,size_k_asim,max_size_k=25
    
    public::get_phase_speed
    
    private::psi,G,count_psi,count_G,count_det_asimetric,count_det_simetric,get_k,size_k_sim,size_k_asim,max_size_k
    contains
    
    subroutine get_phase_speed(speeds)
    use main_data,only:omega
    implicit none
        real(8),allocatable,intent(out)::speeds(:)
        
        integer(4) i
        
        call get_k
        
        call count_psi(k_asim(1))
        call count_G(k_asim(1))
        
        allocate(speeds(size_k_asim+size_k_sim))
        do i=1,size_k_asim
            speeds(i)=omega/k_asim(i)
            !speeds(i)=k_asim(i)
        enddo
        do i=1,size_k_sim
            speeds(size_k_asim+i)=omega/k_sim(i)
            !speeds(size_k_asim+i)=k_sim(i)
        enddo
    endsubroutine get_phase_speed
    
    subroutine get_k
    use Halfc_,only:Halfc
    use Hamin_,only:hamin
    implicit none
        size_k_asim=0
        size_k_sim=0
        
        call Halfc(count_det_asimetric,0d0,18d0,5d-2,1d-5,max_size_k,k_asim,size_k_asim)
        call Halfc(count_det_simetric,0d0,18d0,5d-2,1d-5,max_size_k,k_sim,size_k_sim)
        !call Hamin(count_det_asimetric,0d0,10d0,1d-3,1d-5,max_size_k,k_asim,size_k_asim)
        !call Hamin(count_det_simetric,0d0,10d0,1d-3,1d-5,max_size_k,k_sim,size_k_sim)
    endsubroutine get_k
    
    complex(8) function count_det_asimetric(k) result(f)
    use main_data,only:rho,omega,C,l,epsilon,h,epsilon0
    use math,only:ch,sh,th
    implicit none
        real(8),intent(in)::k
        
        complex(8) M(2,2)
        real(8) d
        
        call count_psi(k)
        call count_G(k)
        
        d=h*0.5d0
        
        M(1,1)=(C(4,4)+G(1)*l(2,4))*psi(1)*ch(psi(1)*d)
        M(1,2)=(C(4,4)+G(3)*l(2,4))*psi(3)*ch(psi(3)*d)
        M(2,1)=(l(2,4)-epsilon(2,2)*G(1))*psi(1)*ch(psi(1)*d)-epsilon0*k*G(1)*sh(psi(1)*d)
        M(2,2)=(l(2,4)-epsilon(2,2)*G(3))*psi(3)*ch(psi(3)*d)-epsilon0*k*G(3)*sh(psi(3)*d)
        
        f=M(1,1)*M(2,2)-M(1,2)*M(2,1)
        
        
        !f=((C(4,4)+l(2,4)*G(1))*psi(1)*(&
        !        (l(2,4)-epsilon(2,2)*G(3))*psi(3)-epsilon0*k*G(3)*th(psi(3)*d)&
        !    )-&
        !    (C(4,4)+l(2,4)*G(3))*psi(3)*(&
        !        (l(2,4)-epsilon(2,2)*G(3))*psi(3)-epsilon0*k*G(1)*th(psi(1)*d)&
        !    ))
    endfunction count_det_asimetric
    complex(8) function count_det_simetric(k) result(f)
    use main_data,only:rho,omega,C,l,epsilon,h,epsilon0
    use math,only:ch,sh,th
    implicit none
        real(8),intent(in)::k
        
        complex(8) M(2,2)
        real(8) d
        
        call count_psi(k)
        call count_G(k)
        
        d=h*0.5d0
        
        M(1,1)=(C(4,4)+G(1)*l(2,4))*psi(1)*sh(psi(1)*d)
        M(1,2)=(C(4,4)+G(3)*l(2,4))*psi(3)*sh(psi(3)*d)
        M(2,1)=(l(2,4)-epsilon(2,2)*G(1))*psi(1)*sh(psi(1)*d)-epsilon0*k*G(1)*ch(psi(1)*d)
        M(2,2)=(l(2,4)-epsilon(2,2)*G(3))*psi(3)*sh(psi(3)*d)-epsilon0*k*G(3)*ch(psi(3)*d)
        
        f=(M(1,1)*M(2,2)-M(1,2)*M(2,1))
        
        !f=abs((C(4,4)+l(2,4)*G(1))*psi(1)*(&
        !        (l(2,4)-epsilon(2,2)*G(3))*psi(3)-epsilon0*k*G(3)/th(psi(3)*d)&
        !    )-&
        !    (C(4,4)+l(2,4)*G(3))*psi(3)*(&
        !        (l(2,4)-epsilon(2,2)*G(3))*psi(3)-epsilon0*k*G(1)/th(psi(1)*d)&
        !    ))
    endfunction count_det_simetric
    
    subroutine count_psi(k)
    use main_data,only:rho,omega,C,l,epsilon
    implicit none
        real(8),intent(in)::k
        
        complex(8) a_,b_,c_
        complex(8) psi12,psi32
        
        a_=C(4,4)*epsilon(2,2)+l(2,4)*l(2,4)
        b_=rho*omega*omega*epsilon(2,2)-2d0*l(2,4)*l(1,5)*k*k-C(4,4)*epsilon(1,1)*k*k-C(5,5)*epsilon(2,2)*k*k
        c_=(C(5,5)*epsilon(1,1)+l(1,5)*l(1,5))*k*k*k*k-rho*omega*omega*epsilon(1,1)*k*k
        
        psi12=(-b_+sqrt(b_*b_-4d0*a_*c_))/a_*0.5d0
        psi32=(-b_-sqrt(b_*b_-4d0*a_*c_))/a_*0.5d0
        
        psi(1)=sqrt(psi12)
        psi(2)=-psi(1)
        psi(3)=sqrt(psi32)
        psi(4)=-psi(3)
    endsubroutine count_psi
    subroutine count_G(k)
    use main_data,only:rho,omega,C,l,epsilon
    implicit none
        real(8),intent(in)::k
        
        integer(4) i
        
        do i=1,4
            G(i)=(l(2,4)*psi(i)*psi(i)-l(1,5)*k*k)/(epsilon(2,2)*psi(i)*psi(i)-epsilon(1,1)*k*k)
        enddo
    endsubroutine  count_G
endmodule group_and_phase_speed